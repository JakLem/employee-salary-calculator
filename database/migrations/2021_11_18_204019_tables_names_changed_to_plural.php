<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablesNamesChangedToPlural extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->rename('employees');
        });
        Schema::table('department', function (Blueprint $table) {
            $table->rename('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->rename('employee');
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->rename('departments');
        });
    }
}
