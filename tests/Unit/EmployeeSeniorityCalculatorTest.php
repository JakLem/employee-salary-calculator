<?php

namespace Tests\Unit;

use App\Src\Domain\EmployeeSeniorityCalculator;
use PHPUnit\Framework\TestCase;

class EmployeeSeniorityCalculatorTest extends TestCase
{
    public function testShouldCalculateTenYearsSeniorityValue()
    {
        $expectedSeniorityValue = 10;
        $seniorityCalculator = new EmployeeSeniorityCalculator();


        $calculatedSeniorityValue = $seniorityCalculator->calculateSeniority(
            1637286363,
            1320102000
        );
        $this->assertEquals($expectedSeniorityValue, $calculatedSeniorityValue);
    }
}
