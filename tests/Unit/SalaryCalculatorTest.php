<?php

namespace Tests\Unit;

use App\Src\SalaryCalculate\DepartmentBonusData;
use App\Src\SalaryCalculate\EmployeeSalaryCalculator;
use PHPUnit\Framework\TestCase;

class SalaryCalculatorTest extends TestCase
{
    public function testShouldCalculateSalaryForEmployeeWithFixedBonus()
    {
        // department data
        $bonusValue = 100.0;
        $bonusType = "fixed";

        // employee data
        $baseSalary = 1000.0;
        $seniority = 15;

        $departmentBonusData = new DepartmentBonusData(
            $bonusValue,
            $bonusType
        );

        $employee = new EmployeeSalaryCalculator(
            $departmentBonusData,
            $baseSalary,
            $seniority
        );

        $calculatedSalary = $employee->calculatedSalary();
        //

        $expectedSalary = 2000.0;
        $this->assertEquals($expectedSalary, $calculatedSalary);
    }
    public function testShouldCalculateSalaryForEmployeeWithPercentBonus()
    {
        // department data
        $bonusValue = 10.0;
        $bonusType = "percent";

        // employee data
        $baseSalary = 1100.0;
        $seniority = 5;

        $departmentBonusData = new DepartmentBonusData(
            $bonusValue,
            $bonusType
        );

        $employee = new EmployeeSalaryCalculator(
            $departmentBonusData,
            $baseSalary,
            $seniority
        );

        $calculatedSalary = $employee->calculatedSalary();
        //

        $expectedSalary = 1210.0;
        $this->assertEquals($expectedSalary, $calculatedSalary);
    }
}
