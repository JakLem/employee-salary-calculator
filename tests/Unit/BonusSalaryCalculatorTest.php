<?php

namespace Tests\Unit;

use App\Src\BonusCalculate\FixedBonusCalculator;
use App\Src\BonusCalculate\PercentBonusCalculator;
use PHPUnit\Framework\TestCase;

class BonusSalaryCalculatorTest extends TestCase
{
    public function testShouldCalculateFixedBonusAmount()
    {
        $expectedBonus = 1000.0;

        /**
         * Zdecydowałem się przekazywać parametr seniority jako parametr metody zamiast konstrutkora.
         * Jest to spowodowane tym, że staż zmienia się częściej niż bonus.
         * Zakładając, że chce przekalkulować listę 30 osób z danego działu to ich bonus jest identyczny.
         * A staż każda z nich może mieć inny.
         */
        $fixedBonusCalculator = new FixedBonusCalculator(bonusPerYear: 100);
        $calculatedBonus = $fixedBonusCalculator->calculateBonus(seniority: 15);

        $this->assertEquals($expectedBonus, $calculatedBonus);
    }

    public function testShouldCalculatePercentBonusAmount()
    {
        $expectedBonus = 120.0;

        /**
         * O tym, w jaki sposób przekazuje parametry, zdecydowało to samo co w teście powyżej.
         */
        $percentBonusCalculator = new PercentBonusCalculator(percentBonusValue: 10);
        $calculatedBonus = $percentBonusCalculator->calculateBonus(baseSalary: 1200);

        $this->assertEquals($expectedBonus, $calculatedBonus);
    }
}
