<?php

namespace Tests\Feature;

use App\Src\Application\EmployeeReport;
use App\Src\Application\EmployeeReportGeneratorFacade;
use App\Src\Application\SalaryDataProvider;
use App\Src\Application\SalaryCalculatingData;
use App\Src\Infrastructure\CalculatingSalaryDataRepository;
use App\Src\Infrastructure\EmployeesDataRepository;
use Tests\TestCase;

class ReportGenerateTest extends TestCase
{
    public function testShouldReturnGeneratedReportForOneEmployee()
    {
        //given
        $calculatingSalaryDataProvider = $this->mock(CalculatingSalaryDataRepository::class);
        $calculatingSalaryDataProvider->shouldReceive('provideCalculatingData')->andReturn(new SalaryCalculatingData(
            "fixed",
            100.0,
            1000,
            1320102000,
        ));

        $employeeDataProvider = $this->mock(EmployeesDataRepository::class);
        $employeeDataProvider->shouldReceive('getAllIds')->andReturn([
            rand(0, 100), rand(5, 55)
        ]);
        $employeeDataProvider->shouldReceive('getEmployeeBaseData')->andReturn([
            'name' => "Adam",
            "baseSalary" => 1000,
            "departmentId" => 2,
            "employmentTime" => 1320102000
        ]);

        $expectedSalaryWithBonus = 2000.0;
        $someRandomEmployeeId = rand(0, 99);
        $employeeReportGenerator = new EmployeeReportGeneratorFacade(
            $calculatingSalaryDataProvider,
            $employeeDataProvider
        );

        /**
         * @var EmployeeReport $employeeReport
         */
        //when
        $employeeReport = $employeeReportGenerator->generateForEmployee($someRandomEmployeeId);

        //then
        $this->assertEquals($expectedSalaryWithBonus, $employeeReport->salaryWithBonus);
    }


    public function testShouldReturnGeneratedReportForOneEmployeeWith5YearsSeniority()
    {
        //given
        $calculatingSalaryDataProvider = $this->mock(CalculatingSalaryDataRepository::class);
        $calculatingSalaryDataProvider->shouldReceive('provideCalculatingData')->andReturn(new SalaryCalculatingData(
            "fixed",
            100.0,
            1000,
            1479504166,
        ));

        $employeeDataProvider = $this->mock(EmployeesDataRepository::class);
        $employeeDataProvider->shouldReceive('getAllIds')->andReturn([
            rand(0, 100), rand(5, 55)
        ]);
        $employeeDataProvider->shouldReceive('getEmployeeBaseData')->andReturn([
            'name' => "Adam",
            "baseSalary" => 1000,
            "departmentId" => 2,
            "employmentTime" => 1320102000
        ]);

        $expectedSalaryWithBonus = 1500.0;
        $someRandomEmployeeId = rand(0, 99);
        $employeeReportGenerator = new EmployeeReportGeneratorFacade(
            $calculatingSalaryDataProvider,
            $employeeDataProvider
        );

        /**
         * @var EmployeeReport $employeeReport
         */
        //when
        $employeeReport = $employeeReportGenerator->generateForEmployee($someRandomEmployeeId);

        //then
        $this->assertEquals($expectedSalaryWithBonus, $employeeReport->salaryWithBonus);
    }


    public function testShouldReturnGeneratedReportForOneEmployeeWithPercentBonus()
    {
        //given
        $calculatingSalaryDataProvider = $this->mock(CalculatingSalaryDataRepository::class);
        $calculatingSalaryDataProvider->shouldReceive('provideCalculatingData')->andReturn(new SalaryCalculatingData(
            "percent",
            10.0,
            1100,
            1479504166,
        ));

        $employeeDataProvider = $this->mock(EmployeesDataRepository::class);
        $employeeDataProvider->shouldReceive('getAllIds')->andReturn([
            rand(0, 100), rand(5, 55)
        ]);
        $employeeDataProvider->shouldReceive('getEmployeeBaseData')->andReturn([
            'name' => "Adam",
            "baseSalary" => 1000,
            "departmentId" => 2,
            "employmentTime" => 1320102000
        ]);

        $expectedSalaryWithBonus = 1210.0;
        $someRandomEmployeeId = rand(0, 99);
        $employeeReportGenerator = new EmployeeReportGeneratorFacade(
            $calculatingSalaryDataProvider,
            $employeeDataProvider
        );

        /**
         * @var EmployeeReport $employeeReport
         */
        //when
        $employeeReport = $employeeReportGenerator->generateForEmployee($someRandomEmployeeId);

        //then
        $this->assertEquals($expectedSalaryWithBonus, $employeeReport->salaryWithBonus);
    }
}
