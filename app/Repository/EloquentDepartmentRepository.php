<?php

namespace App\Repository;

use App\Models\Department;
use App\Src\Infrastructure\DepartmentModelSnapshot;

class EloquentDepartmentRepository
{
    public function findById(int $departmentId): DepartmentModelSnapshot
    {
        $department = Department::find($departmentId)->toArray();

        $snapshot = new DepartmentModelSnapshot();
        $snapshot->bonusType = $department['bonus_type'];
        $snapshot->bonusValue = $department['bonus_value'];

        return $snapshot;
    }
}
