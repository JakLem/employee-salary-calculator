<?php

namespace App\Repository;

use App\Models\Employee;
use App\Src\Infrastructure\EmployeeModelSnapshot;

class EloquentEmployeeRepository
{
    public function findById(int $employeeId): EmployeeModelSnapshot
    {
        $employee = Employee::find($employeeId)->toArray();

        $snapshot = new EmployeeModelSnapshot();
        $snapshot->baseSalary = $employee['base_salary'];
        // SQLITE(?) Problem, thats why i have to devide it
        $snapshot->employmentTime = $employee['employment_time']/1000;
        $snapshot->departmentId = $employee['department_id'];
        $snapshot->name = $employee['name'];

        return $snapshot;
    }

    public function getAllIds(): array
    {
        return Employee::all()->pluck('id')->toArray();
    }
}
