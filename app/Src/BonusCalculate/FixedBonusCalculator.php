<?php

namespace App\Src\BonusCalculate;

class FixedBonusCalculator
{
    public function __construct(
        private float $bonusPerYear
    ) {
        if ($this->bonusPerYear < 0) {
            throw new \Exception("bonus cannot be lower than 0");
        }
    }

    public function calculateBonus(int $seniority): float
    {
        if ($seniority > 10) {
            $seniority = 10;
        }

        return $this->bonusPerYear * $seniority;
    }
}
