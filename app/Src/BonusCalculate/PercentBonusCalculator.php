<?php

namespace App\Src\BonusCalculate;

class PercentBonusCalculator
{

    /**
     * @param int $int
     */
    public function __construct(
        private float $percentBonusValue
    ) {
        if ($this->percentBonusValue < 0) {
            throw new \Exception("percent cannot be lower than 0");
        }
    }

    public function calculateBonus(float $baseSalary): float
    {
        return $baseSalary * ($this->percentBonusValue / 100);
    }
}
