<?php

namespace App\Src\Application;

interface EmployeesDataProvider
{
    public function getAllIds(): array;
    public function getEmployeeBaseData(int $id): array;
}
