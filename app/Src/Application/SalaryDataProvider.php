<?php

namespace App\Src\Application;

interface SalaryDataProvider
{
    public function provideCalculatingData(int $employeeId): SalaryCalculatingData;
}
