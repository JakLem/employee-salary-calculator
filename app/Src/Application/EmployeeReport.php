<?php

namespace App\Src\Application;

class EmployeeReport
{
    public string $name;
    public float $baseSalary;
    public float $salaryWithBonus;
}
