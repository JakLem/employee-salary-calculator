<?php

namespace App\Src\Application;

use App\Src\SalaryCalculate\DepartmentBonusData;
use App\Src\SalaryCalculate\EmployeeSalaryCalculator;

/**
 * To refactor this class (probably) should be devide for two classes.
 * One for all employees and second for one employee.
 */
class EmployeeReportGeneratorFacade
{
    public function __construct(
        private SalaryDataProvider $salaryDataProvider,
        private EmployeesDataProvider $employeesDataProvider
    ) {
    }

    public function generateForEmployee(int $employeeId): EmployeeReport
    {
        /** @var SalaryCalculatingData $salaryCalculatingData */
        $salaryCalculatingData = $this->salaryDataProvider->provideCalculatingData($employeeId);

        $departmentBonusData = new DepartmentBonusData(
            $salaryCalculatingData->departmentBonusValue,
            $salaryCalculatingData->departmentBonusType
        );
        $calculator = new EmployeeSalaryCalculator(
            $departmentBonusData,
            $salaryCalculatingData->employeeBaseSalary,
            $salaryCalculatingData->employeeSeniority
        );

        $employeeReport = new EmployeeReport();
        $employeeReport->baseSalary = $this->employeesDataProvider->getEmployeeBaseData($employeeId)['baseSalary'];
        $employeeReport->salaryWithBonus = $calculator->calculatedSalary();
        $employeeReport->name = $this->employeesDataProvider->getEmployeeBaseData($employeeId)['name'];

        return $employeeReport;
    }

    public function generateForAll(): array
    {
        $employeesId = $this->employeesDataProvider->getAllIds();

        $reports = [];
        foreach ($employeesId as $employeeId) {
            $reports[] = $this->generateForEmployee($employeeId);
        }

        return $reports;
    }
}
