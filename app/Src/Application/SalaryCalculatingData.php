<?php

namespace App\Src\Application;

use App\Src\Domain\EmployeeSeniorityCalculator;

/**
 * To refactor there should be read only public properties for example by magic __get method
 */
class SalaryCalculatingData
{
    public string $employeeSeniority;

    public function __construct(
        public string $departmentBonusType,
        public float $departmentBonusValue,
        public float $employeeBaseSalary,
        int $employeeEmploymentTime,
    ) {
        $this->employeeSeniority =
            (new EmployeeSeniorityCalculator())->calculateSeniority(time(), $employeeEmploymentTime);
    }
}
