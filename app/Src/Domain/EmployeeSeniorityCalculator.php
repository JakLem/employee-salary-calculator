<?php

namespace App\Src\Domain;

use DateTime;

class EmployeeSeniorityCalculator
{
    public function calculateSeniority(int $nowTimestamp, int $employeeEmploymentTimestamp): int
    {
        $nowData = new DateTime(date('d-m-Y', $nowTimestamp));
        $employmentData = new DateTime(date('d-m-Y', $employeeEmploymentTimestamp));

        $diffTime = $nowData->diff($employmentData);

        return $diffTime->y;
    }
}
