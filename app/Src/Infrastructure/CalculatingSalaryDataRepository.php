<?php

namespace App\Src\Infrastructure;

use App\Repository\EloquentDepartmentRepository;
use App\Repository\EloquentEmployeeRepository;
use App\Src\Application\SalaryCalculatingData;
use App\Src\Application\SalaryDataProvider;

class CalculatingSalaryDataRepository implements SalaryDataProvider
{
    public function __construct(
        private EloquentEmployeeRepository $employeeRepository,
        private EloquentDepartmentRepository $departmentRepository
    ) {
    }

    public function provideCalculatingData(int $employeeId): SalaryCalculatingData
    {
        $employeeSnapshot = $this->employeeRepository->findById($employeeId);
        $departmentSnapshot = $this->departmentRepository->findById($employeeSnapshot->departmentId);

        return new SalaryCalculatingData(
            $departmentSnapshot->bonusType,
            $departmentSnapshot->bonusValue,
            $employeeSnapshot->baseSalary,
            $employeeSnapshot->employmentTime
        );
    }
}
