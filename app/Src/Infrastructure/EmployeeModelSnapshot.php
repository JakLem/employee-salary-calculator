<?php

namespace App\Src\Infrastructure;

class EmployeeModelSnapshot
{
    public string $name;
    public float $baseSalary;
    public float $employmentTime;
    public int $departmentId;
}
