<?php

namespace App\Src\Infrastructure;

use App\Repository\EloquentEmployeeRepository;
use App\Src\Application\EmployeesDataProvider;

class EmployeesDataRepository implements EmployeesDataProvider
{
    public function __construct(
        private EloquentEmployeeRepository $eloquentEmployeeRepository
    ) {
    }

    /**
     * Method for getting all data for specific employee should be refactored.
     * There might be used dto instead of array for transfer employee data.
     * @param int $employeeId
     * @return array
     */
    public function getEmployeeBaseData(int $employeeId): array
    {
        $data = $this->eloquentEmployeeRepository->findById($employeeId);
        return (array)$data;
    }

    public function getAllIds(): array
    {
        return $this->eloquentEmployeeRepository->getAllIds();
    }
}
