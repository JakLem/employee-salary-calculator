<?php

namespace App\Src\SalaryCalculate;

class DepartmentBonusData
{
    public function __construct(
        public float $bonusValue,
        public string $bonusType
    ) {
    }
}
