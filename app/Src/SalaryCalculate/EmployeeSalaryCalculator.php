<?php

namespace App\Src\SalaryCalculate;

use App\Src\BonusCalculate\FixedBonusCalculator;
use App\Src\BonusCalculate\PercentBonusCalculator;

class EmployeeSalaryCalculator
{
    public function __construct(
        private DepartmentBonusData $departmentBonusData,
        private float               $baseSalary,
        private int                 $seniority
    ) {
    }

    public function calculatedSalary(): float
    {
        $calculatedBonus = 0;

        switch ($this->departmentBonusData->bonusType) {
            case "fixed":
                $calculator = new FixedBonusCalculator($this->departmentBonusData->bonusValue);
                $calculatedBonus = $calculator->calculateBonus($this->seniority);
                break;
            case "percent":
                $calculator = new PercentBonusCalculator($this->departmentBonusData->bonusValue);
                $calculatedBonus = $calculator->calculateBonus($this->baseSalary);
                break;
        }

        return $this->baseSalary + $calculatedBonus;
    }
}
