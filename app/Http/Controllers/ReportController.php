<?php

namespace App\Http\Controllers;

use App\Src\Application\EmployeeReportGeneratorFacade;
use App\Src\Infrastructure\CalculatingSalaryDataRepository;
use App\Src\Infrastructure\EmployeesDataRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReportController extends Controller
{
    public function generate(): JsonResponse
    {
        $eloquentCalculatingSalaryDataRepository = app()->make(CalculatingSalaryDataRepository::class);
        $eloquentEmployeeDataRepository = app()->make(EmployeesDataRepository::class);

        $employeeReportGenerator = new EmployeeReportGeneratorFacade(
            $eloquentCalculatingSalaryDataRepository,
            $eloquentEmployeeDataRepository
        );
        $reports = $employeeReportGenerator->generateForAll();
        return new JsonResponse([
            'reports' => $reports
        ]);
    }
}
